import React, { useState } from 'react'
import Pagination from './pagination'
import theme from './theme/normalTheme'

function App() {

  const [selectedPageValue, setSelectedPageValue] = useState(1)

  const handlechange = (selectedPage) => {
    setSelectedPageValue(selectedPage)
    console.log(selectedPage)
  }

  return (
    <>
      <h1>Pagination Compound Components</h1>

      <div>SelectedPageValue (get since App) : {selectedPageValue}</div>
      ...
      ...

      <Pagination theme={theme} onChange={handlechange}>
        <Pagination.Show />

        <Pagination.ArrowBigJump left>&larr;</Pagination.ArrowBigJump>
        <Pagination.ArrowJump left>&lt;</Pagination.ArrowJump>
        <Pagination.Count length={50} itemsPerPage={5}/>
        <Pagination.ArrowJump right>&gt;</Pagination.ArrowJump>
        <Pagination.ArrowBigJump right>&rarr;</Pagination.ArrowBigJump>
        
      </Pagination>
    </>
  );
}

export default App;

import React from 'react'
import { useClickedPage } from './hooks/clickedPage'
import { usePages } from './hooks/pages'

function Show () {
    const { clickedPage } = useClickedPage()
    const { pages } = usePages()

    return <>
        <div>Page click : {clickedPage}</div>
        <div>For : {pages}</div>
    </>
}

export default Show
import styled from 'styled-components'

const StyledSpan = styled.span`
    &:hover {
        background-color: rgb(136, 136, 136, 0.1);
    }
    color: ${props => props.isActive ? "#19BC9C" : "#53555D"};
    background-color: ${props => props.isActive ? "rgb(25, 188, 156, 0.1)" : "#ffffff"};
    cursor: pointer;
    padding-left: 5px;
    padding-right: 5px;

`;



export default StyledSpan
import styled from 'styled-components'

const StyledArrow = styled.span`
    &:disabled {
        background-color: rgb(136, 136, 136, 0.1);
    }
    color: black;
    visibility: ${props => props.isActive ? "" : "hidden"};
    cursor: pointer;
    padding-left: 5px;
    padding-right: 5px;

`;



export default StyledArrow
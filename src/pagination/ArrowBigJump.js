import React from 'react'
import { isActive } from './helpers/isActiveArrow'
import PropTypes from 'prop-types'
import StyledArrow from './styled/StyledArrow'
import { useClickedPage } from './hooks/clickedPage'
import { usePages } from './hooks/pages'


function ArrowBigJump ({left, right, children}) {

    const { clickedPage, setClickedPage } = useClickedPage()
    const { pages } = usePages()

    const handleClick = () => {
        if (left && clickedPage !== 1) {
            setClickedPage(1)
        } else if (right && clickedPage !== pages){
            setClickedPage(pages)
        }
    }

    return (
        <StyledArrow onClick={handleClick} isActive={isActive(left, right, clickedPage, pages)} >
            {children}
        </StyledArrow>
    )
}

ArrowBigJump.propTypes = {
    left: PropTypes.bool,
    right: PropTypes.bool,
    children: PropTypes.node
}


export default ArrowBigJump
export const getNewPages = (clickedPage, length, itemsPerPage) => {

    const pagesCount = length / itemsPerPage
    const pages = []
    
    for (let index = 1; index < pagesCount + 1; index++) {
        pages.push(index)
    }
    
    const visiblePages = []

    for (let index = clickedPage -3; index < clickedPage; index++) {
        if (pages.indexOf(index) !== -1) {
            visiblePages.push(index)
        }
    }
    
    for (let index = clickedPage; index < clickedPage + 4; index++) {
        if (pages.indexOf(index) !== -1) {
            visiblePages.push(index)
        }
    }
    
    return visiblePages
}



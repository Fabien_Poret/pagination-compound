import React, { useEffect } from 'react'
import Span from './Span'
import PropTypes from 'prop-types'
import { getNewPages } from './helpers/getNewPages'
import { useClickedPage } from './hooks/clickedPage'
import { usePages } from './hooks/pages'


function Count ({length, itemsPerPage}) {

    const { clickedPage, onChange } = useClickedPage()
    const { setPages } = usePages()

    useEffect(() => {
        setPages(length / itemsPerPage)
    }, [length, itemsPerPage])

    const visiblePages = getNewPages(clickedPage, length, itemsPerPage)
    return visiblePages.map(page => <Span key={page} onChange={onChange}>{page}</Span>)
}

Count.propTypes = {
    length: PropTypes.number,
    itemsPerPage: PropTypes.number,
}

export default Count
import React, { useContext, useState } from 'react'

const ClickedPageContext = React.createContext()

export function useClickedPage(){
    const state = useContext(ClickedPageContext)
    return state
}

export function ClickedPageProvider({children, onChange}) {
    const [clickedPage, setClickedPage] = useState(1)
    const newSetClickedPage = (pageNumber)  => {
        setClickedPage(pageNumber)
        onChange(pageNumber)
    }
    return <ClickedPageContext.Provider value={{clickedPage, setClickedPage: newSetClickedPage}}>
        {children}
    </ClickedPageContext.Provider>
}
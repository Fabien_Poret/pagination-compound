import React, { useContext, useState } from 'react'

const PagesContext = React.createContext()

export function usePages(){
    const state = useContext(PagesContext)
    return state
}

export function PagesProvider({children}) {
    const [pages, setPages] = useState(1)
    return <PagesContext.Provider value={{pages, setPages}}>
        {children}
    </PagesContext.Provider>
}
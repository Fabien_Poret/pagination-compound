import { isActive } from '../helpers/isActiveArrow'

it('return if arrow is active', () => {
    expect(isActive(true, false, 1, 10)).toEqual(false)
    expect(isActive(false, false, 1, 10)).toEqual(false)
    expect(isActive(true, true, 1, 10)).toEqual(false)
    expect(isActive(false, true, 1, 10)).toEqual(true)

    expect(isActive(true, false, 3, 10)).toEqual(true)
    expect(isActive(false, false, 3, 10)).toEqual(false)
    expect(isActive(true, true, 3, 10)).toEqual(true)
    expect(isActive(false, true, 3, 10)).toEqual(true)
    
    expect(isActive(true, false, 10, 10)).toEqual(true)
    expect(isActive(false, false, 10, 10)).toEqual(false)
    expect(isActive(true, true, 10, 10)).toEqual(true)
    expect(isActive(false, true, 10, 10)).toEqual(false)
})

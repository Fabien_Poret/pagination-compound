import { getNewPages } from '../helpers/getNewPages'

it('count pages since helpers/getNewPages', () => {
    expect(getNewPages(1, 50, 5)).toEqual([1,2,3,4])
    expect(getNewPages(1, 5, 50)).toEqual([1])
    expect(getNewPages(5, 50, 5)).toEqual([2, 3, 4, 5, 6, 7, 8])
    expect(getNewPages(100, 500, 5)).toEqual([97, 98, 99, 100]) 
})
import React, { useEffect } from 'react'

import { useClickedPage } from './hooks/clickedPage'
import StyledSpan from './styled/StyledSpan'

import PropTypes from 'prop-types'

function Span ({children}) {

    const { clickedPage, setClickedPage } = useClickedPage()

    const handleClick = () => {
        setClickedPage(children)
    }

    return (
        <StyledSpan isActive={clickedPage === children} onClick={handleClick}> 
            {children}
        </StyledSpan>
        
    )
}

Span.propTypes = { 
    children: PropTypes.number
}

export default Span
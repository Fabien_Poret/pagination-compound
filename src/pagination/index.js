import React from 'react'

import ArrowBigJump from './ArrowBigJump'
import ArrowJump from './ArrowJump'
import { ClickedPageProvider } from './hooks/clickedPage'
import Count from './Count'
import Show from './Show'


import { PagesProvider } from './hooks/pages'

import { ThemeProvider } from 'styled-components'
import StyledPagination from './styled/StyledPagination'

function Pagination({ theme, children, onChange}) {

    return (
        <ThemeProvider theme={theme}>
            <StyledPagination>
                <PagesProvider>
                    <ClickedPageProvider onChange={onChange}>
                        {children}
                    </ClickedPageProvider>
                </PagesProvider>
            </StyledPagination>
        </ThemeProvider>
    )
}

// TODO: Proptypes


Pagination.ArrowBigJump = ArrowBigJump
Pagination.ArrowJump = ArrowJump
Pagination.Count = Count
Pagination.Show = Show

export default Pagination